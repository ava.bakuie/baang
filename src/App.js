import { Navigation } from "react-native-navigation";
import { I18nManager } from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';

//local
import { registerScreens } from "./screens";

EStyleSheet.build({
  // Colors
  $bgColor: '#1A1F2B',
  $primaryColor: '#7F8289',
  $whiteColor: '#ffffff',
  $buttonColor: '#DF00FF',
  // Fonts
  $firstFont: 'robotoregular',
  $secondFont: 'robotobold',
  $thirdFont: 'robotolight'
});

I18nManager.allowRTL(false);
registerScreens();

export default class App{
  constructor(){
    this.start()
  }

  start(){
    Navigation.events().registerAppLaunchedListener(() => {
      Navigation.setRoot({
        root: {
          sideMenu: {
            left: {
              component: {
                id: 'menu',
                name: 'Baang.Menu'
              }
            },
            center: {
              stack: {
                id: "mainAppStack",
                options: {
                  topBar: {
                    drawBehind: true,
                    visible: false,
                    animate: false
                  }
                },
                children: [{
                  component: {
                    name: "Baang.Home"
                  }
                }]
              }
            }
          }
        }
      });
    });
  }
}