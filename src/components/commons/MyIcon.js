import React from 'react';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from './selection.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

// My Custom Icon
const MyIcon = ({ name, size, color = "#000", style, onPress}) => {
    return <Icon name={name} size={size} color={color} style={style} onPress={onPress} />;
};

export {MyIcon};
