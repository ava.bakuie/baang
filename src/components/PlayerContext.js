import React, { Component } from "react";
import {Animated, Easing} from 'react-native'
import TrackPlayer from 'react-native-track-player';
const MyContext = React.createContext();

class MyProvider extends Component{

    playPause = async () => {
        totalTime = await TrackPlayer.getDuration();
        currentTime = await TrackPlayer.getPosition();

        if (!this.state.play) {
            TrackPlayer.play();
            this.interval = setInterval(async () => {
                currentTime = await TrackPlayer.getPosition();
                    Animated.timing(this.state.seekAnimation, {
                    toValue: currentTime,
                    duration: 1000,
                    easing: Easing.linear
                }).start();        

                this.setState({
                    width: this.state.seekAnimation.interpolate({
                        inputRange: [0, totalTime],
                        outputRange: ['0%', '100%']
                    })
                })
            }, 1000)
        } else {
            TrackPlayer.pause();
            clearInterval(this.interval)
        }
        this.setState({
            play: !this.state.play
        })
    }
    state = {
        play: false,
        width: 0,
        totalTime: this.totalTime,
        currentTime: this.currentTime,
        playPause: this.playPause,
        getTime: this.getTime,
        seekAnimation: new Animated.Value(0)
    }

    render() {
        return (
            <MyContext.Provider value={this.state}>
                {this.props.children}                
            </MyContext.Provider>
        )
    }
}
export { MyProvider }
export const ContextConsumer = MyContext.Consumer;