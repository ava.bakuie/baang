import React, { Component } from "react";
import { StyleSheet, FlatList, View, Text, Image } from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';

class Players extends Component {

	render() {
       const { img, title, artist } = this.props;
		return (
            <View style={styles.playerList}>
                <Image source={{uri: img}} style={styles.playerImg}/>
                <Text style={styles.playerTitle}>{title}</Text>
                <Text style={styles.playerArtist}>{artist}</Text>
            </View>
        )
	}
}
const styles = EStyleSheet.create({
    $size: 175,
    players: {
    },
    playerList: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 100
    },
    playerImg: {
        width: '$size',
        height: '$size',
        borderRadius: 8,
        marginBottom: 25
    },
    playerTitle: {
        fontSize: '1.5rem',
        fontFamily: '$thirdFont',
        color: '$whiteColor',
        textAlign: 'center'      
    },
    playerArtist: {
        fontSize: '0.8rem',
        fontFamily: '$thirdFont',
        color: '$whiteColor'
    }
    
})            

export { Players };
