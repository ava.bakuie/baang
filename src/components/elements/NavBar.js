import React, { Component } from "react";
import { StyleSheet, FlatList, View, Text, Image, TouchableOpacity } from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';
import { Navigation } from "react-native-navigation";
// Local
import { MyIcon } from "../commons";


class NavBar extends Component {
  
    openMenu = () => {
       Navigation.mergeOptions('menu', {
            sideMenu: {
                left: {
                    visible: true,
                    enabled: true,
                }
            }
       });
    }
	render() {
        const { title, background } = this.props;
		return (
            <View style={[styles.navbar, background]}>
                <TouchableOpacity style={styles.openMenu} onPress={() => this.openMenu()}>
                    <MyIcon 
                    name={"menu"}
                    style={styles.navIcon}
                />
                </TouchableOpacity>
                <Text style={styles.navTitle}>{title}</Text>
                <TouchableOpacity style={styles.openSearch}>
                    <MyIcon 
                        name={"search"}
                        style={[styles.navIcon, styles.searchIcon]}
                    />
                </TouchableOpacity>
            </View>
        )
	}
}
const styles = EStyleSheet.create({
    navbar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        position: 'absolute',
        zIndex: 15,
        paddingHorizontal: 25,
        paddingVertical: 25,
        width: '100%'
    },
    openMenu: {
        padding: 10
    },
    navTitle: {
        color: '$whiteColor',
        fontSize: '0.9rem',
        textAlign: 'center',
        flex: 0.75
    },
    navIcon: { 
        color: '$whiteColor',
        fontSize: '1.75rem',
    },
    openSearch: {
        padding: 10
    },
    searchIcon: {
        fontSize: '1.35rem'
    }
})            

export { NavBar };
