import React, { Component } from "react";
import { StyleSheet, FlatList, View, Text, Image, TouchableHighlight } from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';
import { MyIcon } from "../commons";
import { Navigation } from "react-native-navigation";

class Playlist extends Component {
    _renderItem = ({item}) => (
        <TouchableHighlight onPress={() => this.changeRoute(item)}>
            <View style={styles.item}> 
                <View style={styles.itemImg}>
                    <Image style={{
                            width: '100%',
                            height: '100%'
                        }} source={{uri: item.img}} 
                    />
                </View>
                <View style={styles.itemInfo}>
                    <Text style={styles.itemTitle}>{item.title}</Text>
                    <Text style={styles.itemCaption}>{item.artist}</Text>
                </View>
                <View>
                    <MyIcon
                        name={"play"}
                        style={styles.myIcon}
                    />
                </View>
            </View>
        </TouchableHighlight>
    )
    changeRoute = (item) => {        
        Navigation.push("mainAppStack", {
            component: {
                name: 'Baang.Player',
                options: {
                    layout: {
                        backgroundColor: '#1A1F2B'
                    }
                },
                passProps: {
                    id: item.id,
                    href: item.href,
                    img: item.img,
                    title: item.title,
                    artist: item.artist
                }
            }
        });
    }
	render() {
        const { data } = this.props;
		return (
            <FlatList
                style={styles.playlist}
                numColumns={1}
                contentContainerStyle={{
                   paddingTop: 75
                }}
                data={data}
                renderItem={this._renderItem}
                keyExtractor={(item) => 'item.id'}
            />
        )
	}
}
const styles = EStyleSheet.create({
    playlist: {
        flex: 1
    },
    item: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 20,
        borderBottomColor: 'rgba(127, 130, 137, 0.2)',
        borderBottomWidth: 1,
        marginHorizontal: 25
    },
    itemImg: {
        width: 70,
        height: 70,
        marginRight: 15,
        borderRadius: 50,
        overflow: 'hidden'
    },
    itemInfo: {
        flex: 1
    },
    itemTitle: {
        color: '$whiteColor',
        fontFamily: '$thirdFont',
        fontSize: '1rem'
    },
    itemCaption: {
        color: '$primaryColor',
        fontFamily: '$thirdFont',
        fontSize: '0.75rem'
    },
    myIcon: {
        color: '$whiteColor',
        fontSize: '1.5rem'
    }
})            

export { Playlist };
