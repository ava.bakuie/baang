import React, { Component } from "react";
import { StyleSheet, FlatList, View, Text, Image } from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';
import Svg, { LinearGradient, Stop, Defs, Rect} from 'react-native-svg';
// Local
import { MyIcon } from "../commons";


class Menu extends Component {
  
	render() {
		return (
            <View style={styles.menu}>
                <View style={styles.menuLayer}>
                    <Svg height="100%" width="100%">
                        <Defs>
                            <LinearGradient id="grad" x1="0%" y1="0%" x2="0%" y2="100%">
                                <Stop offset="0%" stopColor="#dd3875" stopOpacity="0" />
                                <Stop offset="100%" stopColor="#562972" stopOpacity="1" />
                            </LinearGradient>
                        </Defs>
                        <Rect x={0} y={0} width="100%" height="100%" fill="url(#grad)" />
                    </Svg>
                </View>
                <View style={styles.menuContent}>
                    <Text style={styles.menuItem}>Home</Text>
                    <Text style={styles.menuItem}>PlayList</Text>
                    <Text style={styles.menuItem}>Player</Text>
                    <Text style={styles.menuItem}>About</Text>
                </View>
            </View>
        )
	}
}
const styles = EStyleSheet.create({
    menu: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'black'        
    },
    menuLayer: {
        width: "100%",
        height: "100%"
    },
    menuContent: {
        position: 'absolute',
        zIndex: 10,
        paddingTop: 50
    },
    menuItem: {
        fontFamily: '$thirdFont',
        fontSize: '1rem',
        margin: 10,
        color: '$whiteColor'
    }

})            

export { Menu };
