import React, { Component } from "react";
import { View, TouchableOpacity } from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';
import TrackPlayer from 'react-native-track-player';

// Local
import { ContextConsumer } from "../PlayerContext";
import { MyIcon } from "../commons";

class PlayerControler extends Component {

	render() {
		return (
			<View>
				<ContextConsumer>
					{(context) => (
						<View style={styles.playerIcon}>
							<MyIcon
								style={styles.controlerIcon}
								name={"repeat"}
							/>
							<MyIcon
								style={styles.controlerIcon}
								name={"back"}
							/>
							<TouchableOpacity onPress={context.playPause}>
								<MyIcon
									style={styles.controlerIcon}
									name={!context.play ? 'play' : 'pause'}
								/>
							</TouchableOpacity>
							<TouchableOpacity onPress={() => {
								this._skipToNext()
							}}>
								<MyIcon
									style={styles.controlerIcon}
									name={"next"}
								/>
							</TouchableOpacity>
							<TouchableOpacity onPress={() => {
								TrackPlayer.skipToPrevious()
							}}>
								<MyIcon
									style={styles.controlerIcon}
									name={"sounds"}
								/>
							</TouchableOpacity>
						</View>
					)}
				</ContextConsumer>
			</View>
        )
	}
}
const styles = EStyleSheet.create({
	playerIcon: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		marginBottom: 50
	},
	controlerIcon: {
		// flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		color: '$whiteColor',
		fontSize: '2rem'
	}
})

export { PlayerControler };
