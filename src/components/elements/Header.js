import React, { Component } from "react";
import { StyleSheet, View, Dimensions } from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';
import Svg, { Defs, Path, LinearGradient, Stop, ClipPath, Image } from 'react-native-svg'
import { MyIcon } from "../commons";

//Variables
const { width } = Dimensions.get("window");
const Svg_Width = width;

class Header extends Component {
    
	render() {
		return (
            <View style={styles.cont}>
                <Svg
                    style={{
                    isolation: 'isolate',
                    position: 'absolute',
                    opacity: 0.9
                    }}
                    width={Svg_Width}
                    height={150}
                    preserveAspectRatio = "none"
                    viewBox = "0 0 375 216.253"
                >
                    <Defs>
                        <LinearGradient
                            id="prefix__a"
                            x1="0%" y1="50%" x2="100%" y2="100%"
                            gradientUnits="objectBoundingBox"
                        >
                            <Stop offset={"0%"} stopColor="#562972" />
                            <Stop offset={"100%"} stopColor="#dd3875" />
                        </LinearGradient>
                    </Defs>
                    <Path
                    data-name="Path 35"
                    d="M-2225.987 5227.414s78.457 44.26 138.71 39.459 108.776-42.672 156.232-39.472 80.156 31.222 80.058 21.516-.133-197.933-.133-197.933l-374.867.014z"
                    transform="translate(2225.987 -5050.984)"
                    fill="url(#prefix__a)"
                    />
                </Svg>
                <Svg
                    style={{
                    isolation: 'isolate',
                    position: 'absolute',
                    opacity: 0.9
                    }}
                    width={Svg_Width}
                    height={150}
                    preserveAspectRatio="none"
                    viewBox = "0 0 375.731 249.258"
                >
                    <Defs>
                        <LinearGradient
                            id="prefix__b"
                            x1="0%" y1="100%" x2="100%" y2="100%"
                            gradientUnits="objectBoundingBox"
                        >
                            <Stop offset={"0%"} stopColor="#562972" />
                            <Stop offset={"100%"} stopColor="#dd3875" />
                        </LinearGradient>
                    </Defs>
                    {/* <ClipPath id="clip"> */}
                        <Path
                            data-name="Path 34"
                            d="M-3351.97 4979.9l375.558.1v237.027s-85.944-79.5-163.649-78.745c-66.987.65-181.31 95.64-211.954 90.686-.251-.038-.072 2.375.016-10.969-.318-70.987.029-238.099.029-238.099z"
                            transform="translate(3352.144 -4979.898)"
                            fill="url(#prefix__b)"
                        />
                    {/* </ClipPath>
                    <Image 
                        clipPath="url(#clip)"
                        href={require('../../assets/images/ghh.png')}
                        width="100%" 
                        height="100%"
                      /> */}
                </Svg>
            </View>
        )
	}
}
const styles = EStyleSheet.create({
    cont: {
        position: 'absolute',
        zIndex: 8,
        width: Svg_Width,
    },
    mask: {
    }
})            

export { Header };
