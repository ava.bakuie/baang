import React, { Component } from "react";
import { StyleSheet, FlatList, View, Text, Image, Dimensions, TouchableOpacity } from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';
import Svg, { LinearGradient, Stop, Defs, Rect} from 'react-native-svg';
import { Navigation } from "react-native-navigation";
//Variables
const { width } = Dimensions.get("window");
const Item_Size = width*4/10;

class Category extends Component {
    _renderItem = ({item}) => (
        <TouchableOpacity onPress={() => this.changeRoute(item)}>
            <View style={styles.categoryList}>
                <View style={styles.gradientLayer}>
                    <Svg height={Item_Size} width={Item_Size}>
                        <Defs>
                            <LinearGradient id="grad" x1="0%" y1="0%" x2="0%" y2="100%">
                                <Stop offset="0%" stopColor={item.firstColor} stopOpacity="0" />
                                <Stop offset="100%" stopColor={item.secondColor} stopOpacity="1" />
                            </LinearGradient>
                        </Defs>
                        <Rect x={0} y={0} width={Item_Size} height={Item_Size} fill="url(#grad)" />
                    </Svg>
                </View>
                <Image style={styles.categoryImg} source={item.img} />
                <Text style={styles.categoryItem}>{item.title}</Text>
            </View>
        </TouchableOpacity>
    )
    changeRoute = (item) => {
        Navigation.push("mainAppStack", {
            component: {
                name: 'Baang.PlayList',                
                options: {
                    layout: {
                        backgroundColor: '#1A1F2B'
                    }
                },
                passProps: {
                    id: item.id,
                    title: item.title
                }
            }
        });
    }
	render() {
		const { data } = this.props;
		return (
            <FlatList 
                style={styles.category}
                numColumns={2}
                contentContainerStyle={{
                    paddingHorizontal: 20,
                    paddingVertical: 20,
                    paddingTop: 155,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
                data={data}
                renderItem={this._renderItem}
                keyExtractor={(item) => item.id}
            />
        )
	}
}
const styles = EStyleSheet.create({
    category: {
        flex: 1
    },
    categoryList: {
        flexDirection: 'row',
        width: Item_Size,
        height: Item_Size,
        margin: 10,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden',
        position: 'relative',
        // elevation: 10
    },
    gradientLayer: {
        width: Item_Size,
        height: Item_Size,
        position: 'absolute',
        zIndex: 10,
        opacity: 0.8
    },
    categoryImg: {
        flex: 1,
        width: '100%',
        height: '100%',
        position: 'absolute'
    },
    categoryItem: {
        position: 'relative',
        zIndex: 20,
        color: '$whiteColor',
        fontFamily: '$firstFont',
        fontSize: 16
    }
})
export { Category };
