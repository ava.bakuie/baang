import React, { Component } from "react";
import { View, TouchableOpacity, Animated } from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';

// Local
import { ContextConsumer } from "../PlayerContext";

class SeekBar extends Component {
	
	render() {
		
		return (
			<ContextConsumer>
				{(context) => {
					return (	
					<TouchableOpacity activeOpacity={1} style={styles.progress}>								
						<View style={styles.progressLine}></View>
						<Animated.View style={[styles.seekLine, {width: context.width}]}></Animated.View>
						<Animated.View style={[styles.progressButton, {left: context.width}]}></Animated.View>
					</TouchableOpacity>
				)
				}}
			</ContextConsumer>
        )
	}
}
const styles = EStyleSheet.create({
	$size: 15,
	progress: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		position: 'relative',
		paddingVertical: 100
	},
	progressLine: {
		position: 'absolute',
		left: 0,
		width: '100%',
		height: 1,
		opacity: 0.3,
		backgroundColor: '$whiteColor',
	},
	seekLine: {
		position: 'absolute',
		left: 0,
		height: 2,
		borderRadius: 100,
		backgroundColor: '$buttonColor',
	},
	progressButton: {
		position: 'absolute',
		width: '$size',
		height: '$size',
		borderRadius: 50,
		backgroundColor: '$buttonColor',
		borderWidth: 2,
		borderColor: 'white'
	}
})

export { SeekBar };
