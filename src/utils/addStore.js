import React from "react";
import { MyProvider } from "../components/PlayerContext";

const addStore = (Component, ...props) => {
	return class App extends React.Component {
		render() {
			return (
				<MyProvider>
					<Component
						{...{
							...this.props,
							...props,
						}}
					/>
				</MyProvider>
			);
		}
	};
};

export { addStore };
