import React, { Component } from "react";
import { View } from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';

// Local
import { Category, Header, NavBar } from "../../components/elements";

// Variables
const data = [{
		id: 4,
		img: require('../../assets/images/pop.png'),
		firstColor: '#362207',
		secondColor: '#240b36',
		title: 'Pop'
	},
	{
		id: 12,
		img: require('../../assets/images/rock.png'),
		firstColor: '#f67e7d',
		secondColor: '#240b36',
		title: 'Rock'
	},
	{
		id: 3,
		img: require('../../assets/images/romantic.png'),
		firstColor: '#ddf516',
		secondColor: '#240b36',
		title: 'Romantic'
	},
	{
		id: 37,
		img: require('../../assets/images/piano.png'),
		firstColor: '#FF0000',
		secondColor: '#240b36',
		title: 'Piano'
	},
	{
		id: 1222,
		img: require('../../assets/images/workout.png'),
		firstColor: '#DF00FF',
		secondColor: '#240b36',
		title: 'Workout'
	},	
	{
		id: 2,
		img: require('../../assets/images/broken.png'),
		firstColor: '#f7ca44',
		secondColor: '#240b36',
		title: 'Broken'
	},
	{
		id: 7,
		img: require('../../assets/images/traditional.png'),
		firstColor: '#2aa9d2',
		secondColor: '#240b36',
		title: 'Traditional'
	},
	{
		id: 30,
		img: require('../../assets/images/wedding.png'),
		firstColor: '#4E9525',
		secondColor: '#240b36',
		title: 'Wedding'
	},
	{
		id: 8,
		img: require('../../assets/images/sad.jpg'),
		firstColor: '#e6e6d4',
		secondColor: '#240b36',
		title: 'Sad'
	},
	{
		id: 35,
		img: require('../../assets/images/brithday.png'),
		firstColor: '#0e0220',
		secondColor: '#240b36',
		title: 'Brithday'
	}
];

class Home extends Component {
	
	render() {
		
		return (
            <View style={styles.home}>
				<NavBar title="Home"/>
				<Header/>
				<Category data={data}/>						
			</View>
        )
	}
}
const styles = EStyleSheet.create({
	home: {
		flex: 1,
		backgroundColor: '$bgColor'
	},
	text: {
		fontFamily: "$secondFont",
		color: '#ffffff'
	}
})

export { Home };
