import React, { Component } from "react";
import { View, ActivityIndicator } from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';
import { Playlist, NavBar } from "../../components/elements";
import axios from "axios";

const cheerio = require('react-native-cheerio')
class PlayList extends Component {
	state = {
		loading: true,
		data: []
	} 

	componentDidMount = () => {
		axios.get(`https://mrtehran.com/playlist/${this.props.id}`).then(res => {
			let data = [];
			const $ = cheerio.load(res.data);
			$(".musicbox-item").map((i, el) => {
				const title = $(el).find(".item-title").text();
				const artist = $(el).find(".item-artist").text();
				const img = $(el).find(".item-thumb").attr("src");
				const href = $(el).find(".mtp-parent a").attr("href");

				data.push({
					id: i,
					title,
					artist,
					img,
					href
				});
			})
			this.setState({
				loading: false,
				data
			})
		})
	}
	render() {
		
		return this.state.loading ? (
			<View 
				style={{flex: 1,
				justifyContent: 'center', alignItems: 'center'}} >
				<ActivityIndicator size={"large"} color="#562972" />
			</View>
			) :
			(
				<View style={styles.list}>
					<Playlist data={this.state.data}/>
					<NavBar title={this.props.title} background={styles.bg}/>					
				</View>
			)
	}
}
const styles = EStyleSheet.create({
	list: {
		flex: 1
	},
	bg: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		position: 'absolute',
		zIndex: 15,
		padding: 15,
		width: '100%',
		backgroundColor: '$bgColor'
	}
})
export { PlayList };
