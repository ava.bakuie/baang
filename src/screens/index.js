import { Navigation } from "react-native-navigation";
//screens
import { Home } from './Home'
import { PlayList } from './PlayList'
import { Player } from './Player'
import { Menu } from '../components/elements/Menu'
import { addStore } from "../utils/addStore";

const registerScreens = () => {
    Navigation.registerComponent(`Baang.Home`, () => addStore(Home));
    Navigation.registerComponent(`Baang.PlayList`, () => addStore(PlayList));
    Navigation.registerComponent(`Baang.Player`, () => addStore(Player));
    Navigation.registerComponent(`Baang.Menu`, () => addStore(Menu));
}

export {
    registerScreens
};
