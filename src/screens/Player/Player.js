import React, { Component } from "react";
import { View, ActivityIndicator } from "react-native";
import EStyleSheet from 'react-native-extended-stylesheet';
import TrackPlayer from 'react-native-track-player';
import axios from "axios";
import { MyProvider } from "../../components/PlayerContext";

// Local
import { Players, PlayerControler, NavBar } from "../../components/elements";
import { SeekBar } from "../../components/elements/SeekBar";

const cheerio = require('react-native-cheerio')

class Player extends Component {
	state = {
		loading: true
	}

	componentDidMount = () => {
		axios.get(`${this.props.href}`).then(res => {
			const $ = cheerio.load(res.data);
			const title = $(".gp-titles-content h1").text();
			const artist = $(".gp-titles-content h2").text().replace('Track ','');
			const img = $(".gp-artwork-content .item-thumb").attr("src");
			const link = $(".dl-modal-link1").attr("href");

			this.setState({
				loading: false,
				title,
				artist,
				img,
				link
			});
			this.setupPlayer();
		})
	}

	setupPlayer = () => {
		const {
			title,
			artist,
			img,
			link
		} = this.state;

		TrackPlayer.setupPlayer().then(async () => {
			await TrackPlayer.add({
				id: link,
				url: link,
				title,
				artist,
				artwork: img
			})
		});
	}

	render() {		
		return this.state.loading ? (
				<View 
					style={{flex: 1,
					justifyContent: 'center', alignItems: 'center'}} >
					<ActivityIndicator size={"large"} color="#562972" />
				</View>
				) :
				(
				<View>
					<NavBar title={this.props.title}/>
					<View style={styles.player}>
						<Players artist={this.state.artist} title={this.state.title} img={this.state.img}/>
						<SeekBar/>
						<PlayerControler/>					
					</View>
				</View>
		)
	}
}
const styles = EStyleSheet.create({
	player: {
		paddingHorizontal: 35
	}
})

export { Player };
